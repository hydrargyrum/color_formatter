#!/usr/bin/env pytest

import pytest

from color_formatter import ColorFormatter


@pytest.fixture()
def fmt():
    class MockFormatter(ColorFormatter):
        def _colored(self, text, color, on_color, attrs):
            desc = f"color={color},on_color={on_color},attrs={attrs}"
            return f"[begin,{desc}]{text}[end,{desc}]"

    return MockFormatter()


@pytest.mark.parametrize(
    "fstring,args,kwargs,expected",
    [
        (
            "foo bar", (), {},
            "foo bar",
        ),
        (
            "{1} {0}", ("bar", "foo"), {},
            "foo bar",
        ),
        (
            "{f} {b}", (), {"f": "foo", "b": "bar"},
            "foo bar",
        ),
        (
            "{f:5s} {b}", (), {"f": "foo", "b": "bar"},
            "foo   bar",
        ),
        (
            "{f:@red} {b}", (), {"f": "foo", "b": "bar"},
            "[begin,color=red,on_color=None,attrs=None]foo[end,color=red,on_color=None,attrs=None] bar",
        ),
        (
            "{b} {f:@:on_green:}", (), {"f": "foo", "b": "bar"},
            "bar [begin,color=None,on_color=on_green,attrs=None]foo[end,color=None,on_color=on_green,attrs=None]",
        ),
        (
            "{b} {f:@::bold,reverse}", (), {"f": "foo", "b": "bar"},
            "bar [begin,color=None,on_color=None,attrs=['bold', 'reverse']]foo[end,color=None,on_color=None,attrs=['bold', 'reverse']]",
        ),
        (
            "{b} {f:5s@red:on_green:bold}", (), {"f": "foo", "b": "bar"},
            "bar [begin,color=red,on_color=on_green,attrs=['bold']]foo  [end,color=red,on_color=on_green,attrs=['bold']]",
        ),
        (
            "{n:+.2f@{ncolor}} {f:5s@red:{fbg}}", (), {"f": "foo", "b": "bar", "ncolor": "blue", "fbg": "on_yellow", "n": 42},
            "[begin,color=blue,on_color=None,attrs=None]+42.00[end,color=blue,on_color=None,attrs=None] [begin,color=red,on_color=on_yellow,attrs=None]foo  [end,color=red,on_color=on_yellow,attrs=None]",
        ),
    ]
)
def test_basic(fmt, fstring, args, kwargs, expected):
    assert fmt.format(fstring, *args, **kwargs) == expected
