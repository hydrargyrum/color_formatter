# color_formatter

`color_formatter` is a library to use standard Python string formatting with termcolor or other libs.

## Description

Classic color-formatting Python libraries are used like this:

    from termcolor import colored

    print("Some {0} text".format(colored("bad", "red")))

... which is not elegant, and makes alignment even worse:

    print("| {} | bar |".format(colored("{:16s}".format("foo"), "green")))

With `color_formatter`, the color info is inside the format string:

    from color_formatter import StringColorFormatter as cstr

    print(cstr("Some {0:@red} text").format("bad"))

or

    print(cstr("| {:16s@green} | bar |").format("foo"))

## Dependencies

To do the coloring, `color_formatter` uses `termcolor` internally, but it's easy to make it use another library.

