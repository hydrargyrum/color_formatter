# this file is licensed under the WTFPLv2, see COPYING.WTFPL file for details

from string import Formatter


class ColorFormatter(Formatter):
    def _colored(self, text, color, on_color, attrs):
        """reimplement this method to use other than termcolor"""

        from termcolor import colored

        return colored(text, color, on_color, attrs)

    def format_field(self, obj, spec):
        spec, _, color_spec = spec.partition('@')
        ret = super().format_field(obj, spec)

        if color_spec:
            parts = color_spec.split(':')

            color = parts[0] or None
            try:
                on_color = parts[1] or None
            except IndexError:
                on_color = None
            try:
                attrs = parts[2] or None
            except IndexError:
                attrs = None
            else:
                if attrs:
                    attrs = attrs.split(',')

            ret = self._colored(ret, color, on_color, attrs)

        return ret


class StringColorFormatter(str):
    def format(self, *args, **kwargs):
        return ColorFormatter().format(self, *args, **kwargs)
